Drakkar-Sherlokk - Azure QA Deployment

Please follow the following procedure to deploy to a QA environment

* Login to Azure using de CLI
    azure login
    azure config mode arm
    azure account set <subscription name>

* If the resource group is not created:

    azure group create <group name> eastca

* Run the provisionning 

    azure group deployment create -g <group name> -f azuredeploy-elasticsearch.json






